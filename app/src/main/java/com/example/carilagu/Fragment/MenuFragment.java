package com.example.carilagu.Fragment;

import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.carilagu.MainActivity;
import com.example.carilagu.R;

public class MenuFragment extends Fragment implements View.OnClickListener {
    public FragmentListener listener;
    TextView tv_judul;
    Button btn_artis, btn_album, btn_judul;
    ImageView img_judul;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        } else{
            throw new ClassCastException(context.toString()
                    + "must implement FragmentListener");
        }
    }
    public MenuFragment (){

    }

    public static MenuFragment newInstance(String title){
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        args.putString("title",title);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        this.tv_judul = view.findViewById(R.id.judul_menu);
        this.img_judul = view.findViewById(R.id.image_menu);
        this.btn_album = view.findViewById(R.id.btn_album);
        this.btn_artis = view.findViewById(R.id.btn_artis);
        this.btn_judul = view.findViewById(R.id.btn_judul);

        this.btn_judul.setOnClickListener(this);
        this.btn_artis.setOnClickListener(this);
        this.btn_album.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        if(this.btn_judul.getId() == v.getId()){
            listener.changePage(5);
        }
    }
}
