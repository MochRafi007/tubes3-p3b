package com.example.carilagu.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.carilagu.MainActivity;
import com.example.carilagu.R;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkButtonBuilder;
import com.varunest.sparkbutton.SparkEventListener;

public class LeftFragment extends Fragment {
    protected Button btTutorial;
    protected Button btSetting;
    protected Button btnExit;
    protected Button btnHome;
    protected Button btnSearch;
    protected Button btnShare;
    public LeftFragment(){

    }

    public static LeftFragment newInstance(String title){
        LeftFragment fragment = new LeftFragment();
        Bundle args = new Bundle();
        args.putString("title",title);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_left,container,false);
        this.btTutorial = view.findViewById(R.id.btn_tutorial);
        this.btSetting = view.findViewById(R.id.btn_setting);
        this.btnExit = view.findViewById(R.id.btn_exit);
        this.btnHome = view.findViewById(R.id.btn_home);
        this.btnSearch = view.findViewById(R.id.btn_searchbar);
        this.btnShare = view.findViewById(R.id.btn_share);
        this.btTutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity)getActivity();
                mainActivity.changePage(1);
            }
        });
        this.btSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity)getActivity();
                mainActivity.changePage(2);
            }
        });
        this.btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
        this.btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity)getActivity();
                mainActivity.changePage(3);
            }
        });
        this.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mainActivity = (MainActivity)getActivity();
                mainActivity.changePage(5);
            }
        });
        this.btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog2();
            }
        });
        return view;
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this.getContext());

        // set title dialog
        alertDialogBuilder.setTitle("Close Application?");

        // set pesan dari dialog
        alertDialogBuilder
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        MainActivity mainActivity = (MainActivity)getActivity();
                        mainActivity.closeApplication();
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void showDialog2(){
        final SparkButton button1 = new SparkButtonBuilder(getContext())
                .setActiveImage(R.drawable.twitterlogo)
                .setInactiveImage(R.drawable.twitter)
                .setPrimaryColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .setSecondaryColor(ContextCompat.getColor(getContext(), R.color.colorAccent))
                .build();
        final SparkButton button2 = new SparkButtonBuilder(getContext())
                .setActiveImage(R.drawable.fblogo)
                .setInactiveImage(R.drawable.facebook)
                .setPrimaryColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .setSecondaryColor(ContextCompat.getColor(getContext(), R.color.colorAccent))
                .build();
        final SparkButton button3 = new SparkButtonBuilder(getContext())
                .setActiveImage(R.drawable.wa)
                .setInactiveImage(R.drawable.whatsapp)
                .setPrimaryColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .setSecondaryColor(ContextCompat.getColor(getContext(), R.color.colorAccent))
                .build();
        DialogPlus dialog = DialogPlus.newDialog(this.getContext())
                .setContentHolder(new ViewHolder(R.layout.content))
                .setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(DialogPlus dialog, View view) {
                        if(button1.getId() == view.getId()){
                            button1.setChecked(true);
                            button1.playAnimation();
                        }else if(button2.getId() == view.getId()){
                            button2.playAnimation();
                        }else if(button3.getId() == view.getId()){
                            button3.playAnimation();
                        }
                    }
                })
                .setExpanded(true)
                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .create();
        dialog.show();
    }
}
