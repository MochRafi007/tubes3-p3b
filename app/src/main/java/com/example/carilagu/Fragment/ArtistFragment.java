package com.example.carilagu.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.carilagu.R;

public class ArtistFragment extends Fragment {

    public ArtistFragment (){

    }

    public static ArtistFragment newInstance(String title){
        ArtistFragment fragment = new ArtistFragment();
        Bundle args = new Bundle();
        args.putString("title",title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_artist,container,false);
        return view;
    }
}
