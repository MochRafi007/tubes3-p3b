package com.example.carilagu.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.carilagu.R;

public class AlbumFragment extends Fragment {
    public AlbumFragment (){

    }

    public static AlbumFragment newInstance(String title){
        AlbumFragment fragment = new AlbumFragment();
        Bundle args = new Bundle();
        args.putString("title",title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_albums,container,false);
        return view;
    }
}
