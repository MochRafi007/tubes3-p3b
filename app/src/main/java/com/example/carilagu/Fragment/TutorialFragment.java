package com.example.carilagu.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.carilagu.R;

public class TutorialFragment extends Fragment {
    FragmentListener listener;
    TextView tv_judul_tutorial, tv_tutorial1, tv_tutorial2, tv_tutorial3, tv_step1, tv_step2, tv_step3, tv_step_album1, tv_step_album2, tv_step_artis1, tv_step_artis2;
    public TutorialFragment (){

    }

    public static TutorialFragment newInstance(String title){
        TutorialFragment fragment = new TutorialFragment();
        Bundle args = new Bundle();
        args.putString("title",title);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        } else{
            throw new ClassCastException(context.toString()
                    + "must implement FragmentListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_tutorial, container, false);
        this.tv_judul_tutorial = view.findViewById(R.id.tv_judul_tutorial);
        this.tv_tutorial1 = view.findViewById(R.id.tv_tutorial1);
        this.tv_tutorial2 = view.findViewById(R.id.tv_tutorial2);
        this.tv_tutorial3 = view.findViewById(R.id.tv_tutorial3);
        this.tv_step1 = view.findViewById(R.id.tv_step1);
        this.tv_step2 = view.findViewById(R.id.tv_step2);
        this.tv_step3 = view.findViewById(R.id.tv_step3);
        this.tv_step_album1 = view.findViewById(R.id.tv_step_album1);
        this.tv_step_album2 = view.findViewById(R.id.tv_step_album2);
        this.tv_step_artis1 = view.findViewById(R.id.tv_step_artis1);
        this.tv_step_artis2 = view.findViewById(R.id.tv_step_artis2);
        return view;
    }
}
