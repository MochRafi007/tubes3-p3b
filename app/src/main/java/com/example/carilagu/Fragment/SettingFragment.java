package com.example.carilagu.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.carilagu.MainActivity;
import com.example.carilagu.R;

public class SettingFragment extends Fragment {
    FragmentListener listener;
    protected Button btnDark;
    protected Button btnLight;
    protected Button btnClear;
    public SettingFragment (){

    }

    public static SettingFragment newInstance(String title){
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        args.putString("title",title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof FragmentListener){
            this.listener = (FragmentListener) context;
        } else{
            throw new ClassCastException(context.toString()
                    + "must implement FragmentListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_setting, container, false);
        this.btnDark = view.findViewById(R.id.btn_dark);
        this.btnLight = view.findViewById(R.id.btn_light);
        this.btnClear = view.findViewById(R.id.btn_clear);

        this.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        return view;
    }

    private void showDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this.getContext());
        alertDialogBuilder.setTitle("Clear Search History?");
        alertDialogBuilder
                .setMessage("This can't be undo")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        MainActivity mainActivity = (MainActivity)getActivity();
                        mainActivity.closeApplication();
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
