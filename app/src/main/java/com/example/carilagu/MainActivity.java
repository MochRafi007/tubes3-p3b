package com.example.carilagu;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.carilagu.Fragment.FragmentListener;
import com.example.carilagu.Fragment.MenuFragment;
import com.example.carilagu.Fragment.SearchFragment;
import com.example.carilagu.Fragment.SettingFragment;
import com.example.carilagu.Fragment.TutorialFragment;

public class MainActivity extends AppCompatActivity implements FragmentListener {
    public FragmentManager fragmentManager;
    protected MenuFragment main_menu;
    protected TutorialFragment tutorialFragment;
    protected SettingFragment settingFragment;
    protected SearchFragment searchFragment;
    public Toolbar toolbar;
    public DrawerLayout drawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.drawer = findViewById(R.id.drawer_layout);
        this.toolbar = findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        ActionBarDrawerToggle abdt = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.openDrawer,R.string.closeDrawer);
        this.drawer.addDrawerListener(abdt);
        abdt.syncState();
        this.fragmentManager = this.getSupportFragmentManager();
        this.main_menu = MenuFragment.newInstance("New Main Fragment");
        this.tutorialFragment = TutorialFragment.newInstance("New Main Fragment");
        this.settingFragment = SettingFragment.newInstance("New Main Fragment");
        this.searchFragment = SearchFragment.newInstance("New Main Fragment");
        FragmentTransaction ft = this.fragmentManager.beginTransaction();
        ft.add(R.id.fragment_container,this.main_menu)
                .addToBackStack(null)
                .commit();
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.name));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    @Override
    public void changePage(int page){
        FragmentTransaction ft = this.fragmentManager.beginTransaction();
        if (page==1){ //halaman tutorial
            if (this.tutorialFragment.isAdded()){
                ft.show(this.tutorialFragment);
            }else{
                ft.add(R.id.fragment_container,this.tutorialFragment);
            }
            if (this.main_menu.isAdded()){
                ft.hide(this.main_menu);
            }
            if (this.settingFragment.isAdded()){
                ft.hide(this.settingFragment);
            }
            if (this.searchFragment.isAdded()){
                ft.hide(this.searchFragment);
            }
            this.drawer.closeDrawers();
        }else if (page==2){ //halaman setting
            if (this.settingFragment.isAdded()){
                ft.show(this.settingFragment);
            }else{
                ft.add(R.id.fragment_container,this.settingFragment);
            }
            if (this.main_menu.isAdded()){
                ft.hide(this.main_menu);
            }
            if (this.tutorialFragment.isAdded()){
                ft.hide(this.tutorialFragment);
            }
            if (this.searchFragment.isAdded()){
                ft.hide(this.searchFragment);
            }
            this.drawer.closeDrawers();
        }else if (page==3){ //halaman main menu
            if (this.main_menu.isAdded()){
                ft.show(this.main_menu);
            }else{
                ft.add(R.id.fragment_container,this.main_menu);
            }
            if (this.settingFragment.isAdded()){
                ft.hide(this.settingFragment);
            }
            if (this.tutorialFragment.isAdded()){
                ft.hide(this.tutorialFragment);
            }
            if (this.searchFragment.isAdded()){
                ft.hide(this.searchFragment);
            }
            this.drawer.closeDrawers();
        }else if (page==4){ //halaman akun
            if (this.main_menu.isAdded()){
                ft.show(this.main_menu);
            }else{
                ft.add(R.id.fragment_container,this.main_menu);
            }
            if (this.main_menu.isAdded()){
                ft.hide(this.main_menu);
            }
            if (this.searchFragment.isAdded()){
                ft.hide(this.searchFragment);
            }
            this.drawer.closeDrawers();
        }else if (page==5){ //halaman search
            if (this.searchFragment.isAdded()){
                ft.show(this.searchFragment);
            }else{
                ft.add(R.id.fragment_container,this.searchFragment);
            }
            if (this.main_menu.isAdded()){
                ft.hide(this.main_menu);
            }
            if (this.tutorialFragment.isAdded()){
                ft.hide(this.tutorialFragment);
            }
            if (this.settingFragment.isAdded()){
                ft.hide(this.settingFragment);
            }
            this.drawer.closeDrawers();
        }
        ft.commit();
    }

    public void closeApplication(){
        this.moveTaskToBack(true);
        this.finish();
    }
}