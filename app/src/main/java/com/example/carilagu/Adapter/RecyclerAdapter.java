package com.example.carilagu.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.carilagu.Fragment.AlbumFragment;
import com.example.carilagu.Fragment.FragmentListener;
import com.example.carilagu.Model.Dummy;
import com.example.carilagu.R;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_search_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ((ViewHolder) holder).bindView(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                AlbumFragment albumFragment = new AlbumFragment();
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.rec,albumFragment).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return Dummy.judul.length;
    }

//    @Override
//    public Filter getFilter() {
//        return filter;
//    }

//    Filter filter = new Filter() {
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//            List<String> filteredList = new ArrayList<>();
//            if(constraint.toString().isEmpty()){
//                filteredList.addAll(listLaguAll);
//            }else{
//                for(String lagu: listLaguAll){
//                    if(lagu.toLowerCase().contains(constraint.toString().toLowerCase())){
//                        filteredList.add(lagu);
//                    }
//                }
//            }
//            FilterResults filterResults = new FilterResults();
//            filterResults.values = filteredList;
//            return null;
//        }
//
////        @Override
////        protected void publishResults(CharSequence constraint, FilterResults results) {
//////            listLagu.clear();
//////            listLagu.addAll((Collection<? extends String>) results.values);
//////            notifyDataSetChanged();
////        }
//    };

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView judulLagu;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.img_search);
            judulLagu = itemView.findViewById(R.id.tv_judul);

        }
        public void bindView(int position){
             judulLagu.setText(Dummy.judul[position]);
             imageView.setImageResource(Dummy.gambar[position]);
        }
    }
}
