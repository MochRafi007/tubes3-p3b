//package com.example.carilagu.Adapter;
//
//import android.app.Activity;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.example.carilagu.R;
//
//import org.json.JSONException;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ListLagu extends BaseAdapter {
//    private List<Lagu> listItems;
//    private Activity activity;
//    private MainPresenter mainPresenter;
//
//    public ListLagu(Activity activity,MainPresenter mainPresenter){
//        this.activity = activity;
//        this.listItems = new ArrayList<>();
//        this.mainPresenter = mainPresenter;
//    }
//    public void update(List<Lagu> lagu) {
//        this.listItems.clear();
//        this.listItems.addAll(lagu);
//        this.notifyDataSetChanged();
//    }
//
//    @Override
//    public int getCount() {
//        return listItems.size();
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return listItems.get(i);
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return 0;
//    }
//
//    @Override
//    public View getView(int i, View convertView, ViewGroup parent) {
//
//        ListAlbum.ViewHolder viewHolder;
//        if (convertView == null) {
//            convertView = LayoutInflater.from(this.activity).inflate(R.layout.list_item_group, parent, false);
//            viewHolder = new ListAlbum.ViewHolder(convertView,this.mainPresenter);
//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ListAlbum.ViewHolder) convertView.getTag();
//        }
//        viewHolder.updateView((Artis)this.getItem(i),i);
//        return convertView;
//
//    }
//
//    private class ViewHolder implements View.OnClickListener {
//        ImageView image_lagu;
//        TextView judul;
//        TextView artis;
//        int position;
//        Lagu lagu;
//
//        public ViewHolder(View convertView, MainPresenter mainPresenter) {
//            this.image_lagu = convertView.findViewById(R.id.image_lagu);
//            this.judul = convertView.findViewById(R.id.judul);
//            this.artis = convertView.findViewById(R.id.artis);
//        }
//
//
//        public void updateView(Lagu lagu, int position) {
//            this.position = position;
////            this.namaAlbum.setOnClickListener(this);
//            this.judul.setText((artis.getNama_artis()));
//            this.artis.setText((lagu.getName_artis()));
//            this.lagu = lagu;
//        }
//
//        @Override
//        public void onClick(View v) {
////            if (v.getId() == nama_artis.getId()) {
////                Log.d("size", "onClick: " + getItem(position));
////                this.presenter.detailsMenu(this.artis, this.position);
////                this.presenter.changePage(4);
////            }
////            else if (v.getId() == edit.getId()) {
////                this.presenter.detailsMenu(this.album, position);
////                this.presenter.changePage(5);
////            }
//        }
//    }
//}
