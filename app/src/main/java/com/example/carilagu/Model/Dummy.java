package com.example.carilagu.Model;

import com.example.carilagu.R;

public class Dummy {
    public static String[] judul = new String[]{
            "Bohemian Rhapsody",
            "Scars",
            "Believer",
            "Not Today",
            "Thunder",
    };

    public static int[] gambar = new int[]{
            R.drawable.believer,
            R.drawable.bohemian,
            R.drawable.scars,
            R.drawable.thunder,
            R.drawable.nottoday
    };
}
